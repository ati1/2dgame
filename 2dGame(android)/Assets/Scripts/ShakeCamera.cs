﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour
{
    public static ShakeCamera Instance;
    public float shake = 0;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
    Vector3 start;
    
    void Awake()
    {
        Instance = this;
        start = Camera.main.transform.localPosition;
    }

    void Update()
    {
        if (shake > 0)
        {
            Camera.main.transform.localPosition = new Vector3(Random.insideUnitCircle.x, Random.insideUnitCircle.y, -12) * shakeAmount;
            shake -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            if (Camera.main.transform.localPosition != start)
                Camera.main.transform.localPosition = start;
            shake = 0.0f;
        }
    }

}
