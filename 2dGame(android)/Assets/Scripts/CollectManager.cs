﻿using UnityEngine;
using System.Collections;

public class CollectManager : MonoBehaviour
{
    public static CollectManager Instance;

    public CollectColor CurrentCollectColor;

    public float collectTime;
    private float collectTimer;

    public bool CanCollect;

    public Collect changeCollectPrefab;

    public float spawnTime;
    private float spawnTimer;

    public bool runSpawnTimer;

    public Color[] CollectColors;

    private bool swap;

    void Awake()
    {
        Instance = this;
        CurrentCollectColor = (CollectColor)Random.Range(0, 6);
        Vector2 pos = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));
        SpawnCollectPrefab(CurrentCollectColor, new Vector3(pos.x, pos.y, -2));
    }

    void Update()
    {
        if (collectTimer < collectTime)
        {
            collectTimer += Time.deltaTime;
            CanCollect = true;
            swap = true;
        }
        else
        {
            if (swap)
            {
                FindObjectOfType<Movement>().GetComponent<SpriteRenderer>().color = Color.white;
                FindObjectOfType<Movement>().GetComponent<TrailRenderer>().material.color = Color.white;
                swap = false;
            }
                
            CanCollect = false;
        }

        if (runSpawnTimer)
            spawnTimer += Time.deltaTime;

        if(spawnTimer >= spawnTime && runSpawnTimer)
        {
            runSpawnTimer = false;
            spawnTimer = 0;
            PickCC(CurrentCollectColor);
            Vector2 pos = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));
            SpawnCollectPrefab(CurrentCollectColor, new Vector3(pos.x, pos.y, 0));
        }
    }

    public void PickCC(CollectColor lastCC)
    {
        int r = Random.Range(0, 6);

        while (r == (int)lastCC)
            r = Random.Range(0, 6);

        CurrentCollectColor = (CollectColor)r;
    }

    public void SpawnCollectPrefab(CollectColor cc, Vector3 pos)
    {
        Collect c = GameObject.Instantiate(changeCollectPrefab, pos, changeCollectPrefab.transform.rotation) as Collect;
        c.cc = cc;
        c.GetComponent<SpriteRenderer>().color = CollectColors[(int)cc];
    }

    public void SetCollectColor(CollectColor cc, float time)
    {
        CurrentCollectColor = cc;
        FindObjectOfType<Movement>().GetComponent<SpriteRenderer>().color = CollectColors[(int)cc];
        FindObjectOfType<Movement>().GetComponent<TrailRenderer>().material.color = CollectColors[(int)cc];
        collectTime = time;
        collectTimer = 0;
    }
}

public enum CollectColor
{
    GREEN,
    RED,
    YELLOW,
    BLUE,
    ORANGE,
    PURPLE
};
