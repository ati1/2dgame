﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnotherMenu : MonoBehaviour {

	public Slider slider;
	public int songId;

	public UnityEngine.UI.Text songName;

	public PlaySoundSample samplePlayer;
	
	public Text[] highScoreMesh;
	public Text[] labels;
	public Button[] buttons = null;
	public Button[] optionsButtons = null;
	public bool showMenu;
	bool startShowMenu = true;
	public bool showOptions;
	bool startOptions;
	public bool muted;

	public Vector2 joyStickRightPos;
	public Vector2 joyStickLeftPos;

	public static int difficulty = 0;

	/// <summary>
	/// Changes the song in menu.
	/// </summary>
	public void ChangeSong()
	{
		int id = (int)slider.value;
		songId = id;
		samplePlayer.PlayAudioSample(id);
		songName.text = samplePlayer.clips[id].name;
		PlayerPrefs.SetInt("SongID",id);
	}

	void Awake()
	{
		if (PlayerPrefs.GetFloat("posX") == 0 && PlayerPrefs.GetFloat("posY") == 0)
		{
			ChangeJoyStickPos(joyStickRightPos);
		}
		difficulty = PlayerPrefs.GetInt("difficulty");
		songId =  PlayerPrefs.GetInt("SongID");
		if(slider != null)
			slider.value = songId;
		if(songName != null)
			songName.text = samplePlayer.clips[songId].name;
	}
	
	void Update () 
	{
		if (showMenu)
		{
			showOptions = false;
			if(startShowMenu)
			{
				ClearScores();
				ClearLabels();
				startOptions = true;
				startShowMenu = false;
				ShowMenuButtons();
			}
		}
		if (showOptions) 
		{
			showMenu = false;
			if(startOptions)
			{
				ClearScores();
				ClearLabels();
				startShowMenu = true;
				startOptions = false;
				ShowOptionsButtons();
				GetHighScores();
				ShowScores();
				ShowLabels();
			}
		}

#if UNITY_ANDROID
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position),Camera.main.transform.forward);
			if(hit.collider != null && hit.collider.GetComponent<Button>())
			{
				GameObject buttonGO = hit.collider.gameObject;
				StartCoroutine("DoClick",buttonGO);
			}
		}
#elif UNITY_STANDALONE

        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition),Camera.main.transform.forward);
		if(Input.GetKeyDown(KeyCode.Mouse0) && hit.collider != null && hit.collider.GetComponent<Button>())
		{
			GameObject buttonGO = hit.collider.gameObject;
			StartCoroutine("DoClick",buttonGO);
		}
#endif

    }

    IEnumerator DoClick(GameObject button)
	{
		Color temp = button.GetComponent<SpriteRenderer>().color;
		button.GetComponent<SpriteRenderer>().color = Color.gray;
		yield return new WaitForSeconds(0.1f);
		button.GetComponent<SpriteRenderer>().color = temp;
		yield return new WaitForSeconds(0.1f);
		DoButtonByID(button.GetComponent<Button>().buttonId);
	}
	void GetHighScores()
	{
		int[] highScores = new int[3]
		{
			PlayerPrefs.GetInt ("HighScore1"),
			PlayerPrefs.GetInt ("HighScore2"),
			PlayerPrefs.GetInt ("HighScore3")
			/*PlayerPrefs.GetInt ("HighScore4"),
			PlayerPrefs.GetInt ("HighScore5")*/
		};
		
		if(highScoreMesh != null)
		{
			for(int i = 0;i<highScoreMesh.Length;i++)
			{
				for(int j = 0;j<highScores.Length;j++)
				{
					highScoreMesh[j].gameObject.GetComponent<TextMesh>().text = highScores[j].ToString();
				}
			}
		}
	}
	void ClearScores()
	{
		GameObject[] oldScores = GameObject.FindGameObjectsWithTag("displayScore");
		foreach(GameObject oldgo in oldScores)
		{
			Destroy(oldgo);
		}
	}
	void ShowScores()
	{
		for(int i = 0;i<highScoreMesh.Length;i++)
		{
			GameObject.Instantiate(highScoreMesh[i].gameObject,highScoreMesh[i].pos,Quaternion.identity);
		}
	}
	void ClearLabels()
	{
		Text[] oldTexts = FindObjectsOfType<Text>();
		for (int i = 0; i<oldTexts.Length; i++) 
		{
			if(oldTexts[i].tag != "displayScore")
			{
				Destroy(oldTexts[i].gameObject);
			}
		}
	}
	void ShowLabels()
	{
		for (int i = 0; i<labels.Length; i++) 
		{
			GameObject.Instantiate(labels[i].gameObject,labels[i].transform.position,Quaternion.identity);
		}
	}
	void ShowMenuButtons()
	{
		Button[] oldButtons = FindObjectsOfType<Button>();
		foreach(Button oldButton in oldButtons)
		{
			Destroy(oldButton.gameObject);
		}
		for(int i = 0;i<buttons.Length;i++)
		{
			SpawnButton(buttons[i]);
		}	
	}
	void ShowOptionsButtons()
	{
		showMenu = false;
		startOptions = false;
		Button[] oldButtons = FindObjectsOfType<Button>();
		foreach(Button oldButton in oldButtons)
		{
			Destroy(oldButton.gameObject);
		}
		
		for(int i = 0;i<optionsButtons.Length;i++)
		{
			SpawnButton(optionsButtons[i]);
		}
	}
	int CheckButtonAmount()
	{
		Button[] buttons = FindObjectsOfType<Button>();
		return buttons.Length;
	}
	void SpawnButton(Button button)
	{
		GameObject.Instantiate(button.buttonObj,button.buttonPos,Quaternion.identity);
	}
	public void ChangeJoyStickPos(Vector2 pos)
	{
		PlayerPrefs.SetFloat("posX",pos.x);
		PlayerPrefs.SetFloat("y",pos.y);
	}
	public void Mute()
	{
		if(!muted)
		{
			Debug.Log("Muted");
			muted = true;
		}
		else
		{
			Debug.Log("Not Muted");
			muted = false;
		}
	}
	//logic of specific buttons here
	void DoButtonByID(int id)
	{
		switch(id)
		{
		case 0:
			Application.LoadLevel("Game"); 
			PlayerPrefs.SetInt("difficulty",difficulty);
			break;
		case 1:
			showOptions = true;
			showMenu=false;
			break;
		case 2:
			Application.Quit();
			break;
		case 3:
			Application.LoadLevel("Menu");
			break;
		case 4:
			showMenu = true;
			showOptions=false;break;
		//Right
		case 5:
			ChangeJoyStickPos(joyStickRightPos);
			break;
		//Left
		case 6:
			ChangeJoyStickPos(joyStickLeftPos);
			break;
		case 7:
			difficulty = 0;
			break;
		case 8:
			difficulty = 1;
			break;
		case 9:
			difficulty = 2;
			break;
		case 10:
			difficulty = 3;
			break;
		case 11:
			difficulty = 4;
			break;
		case 12:
			difficulty = 5;
			break;
		case 13:
			ResetData();
			break;
		}
	}
	void ResetData()
	{
		PlayerPrefs.DeleteAll();
		GetHighScores ();
		ClearScores ();
		ShowScores ();
		ChangeJoyStickPos(joyStickRightPos);
	}
}
