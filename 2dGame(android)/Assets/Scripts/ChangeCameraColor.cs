﻿using UnityEngine;
using System.Collections;

public class ChangeCameraColor : MonoBehaviour {

	Movement movement;
	public bool changeColorByTime = false;
	public float timer = 0;
	public float maxTimer = 0;
	

	void Start()
	{
		movement = FindObjectOfType<Movement>();
	}

	void Update () 
	{
		if(changeColorByTime)
		{
			if(timer <= 0)
			{
                //broken :)
                StartCoroutine("FadeColor");
				//GetComponent<Camera>().backgroundColor = Color32.Lerp(GetComponent<Camera>().backgroundColor,movement.GetRandomColor(),Time.deltaTime * 2);
				timer = maxTimer;
			}
			timer -= Time.deltaTime;
		}
		else
		{
#if UNITY_ANDROID
            if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount == 2)
			{
                StartCoroutine("FadeColor");
                //GetComponent<Camera>().backgroundColor = Color32.Lerp(GetComponent<Camera>().backgroundColor,movement.GetRandomColor(),Time.time);
            }
#elif UNITY_STANDALONE
            if(Input.GetKeyDown(KeyCode.R))
			{
                StartCoroutine("FadeColor");
				//GetComponent<Camera>().backgroundColor = Color32.Lerp(GetComponent<Camera>().backgroundColor,movement.GetRandomColor(),Time.time);
			}
#endif
        }

    }

    private IEnumerator FadeColor()
    {
        float i = 0;
        Color32 c = movement.GetRandomColor();
        while (i < 1)
        {
            GetComponent<MenuCamera>().transform.GetChild(0).GetComponent<Renderer>().material.color = Color32.Lerp(GetComponent<MenuCamera>().transform.GetChild(0).GetComponent<Renderer>().material.color, c, i);
            i += Time.deltaTime;
            yield return null;
        }
    }
}