﻿using UnityEngine;
using System.Collections;

public class Joystick : MonoBehaviour {

	public Transform playerTransform;
	public Transform joyStickBg;

	public Vector3 clipPos,joystickMaxSpot, joystickMinSpot;
	public Vector2 joyStickStartPos;
	public Vector2 joyStickPos;

	public float speed = 0;
	
	void Awake () 
	{
		joyStickPos = new Vector3 (PlayerPrefs.GetFloat ("posX"), PlayerPrefs.GetFloat ("y"), -.5f);
		joyStickBg.position = new Vector3(joyStickPos.x,joyStickPos.y,-.1f);
		transform.position = joyStickPos;
		joyStickStartPos = joyStickPos;
		transform.position = clipPos = new Vector3(joyStickStartPos.x,joyStickStartPos.y,-.5f);
	}

	void FixedUpdate () 
	{
		MovePlayer();
		joyStickPos = transform.position;
	}
	void MovePlayer()
	{
#if UNITY_ANDROID
        playerTransform.Translate((MoveJoyStick() - new Vector3(joyStickStartPos.x,joyStickStartPos.y,-.5f)).normalized * 
			                        (joyStickStartPos - joyStickPos).magnitude * Time.deltaTime * speed);
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
			transform.position = clipPos;
#elif UNITY_STANDALONE
        if(Input.GetMouseButton(0))
		{

		    playerTransform.Translate((MoveJoyStick() - new Vector3(joyStickStartPos.x,joyStickStartPos.y,-.5f)).normalized * 
			                        (joyStickStartPos - joyStickPos).magnitude * Time.deltaTime * speed);

		}
		else
        {
            transform.position = clipPos;
        }	
#endif   
	}

    Vector3 MoveJoyStick()
    {
#if UNITY_ANDROID
        //			Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x,Input.GetTouch(0).position.y,0));
        if (Input.touchCount == 0)
            return transform.position;

        /*float tPosX = (Input.touchCount > 0) ? Input.GetTouch(0).position.x : joyStickStartPos.x;
        float tPosY = (Input.touchCount > 0) ? Input.GetTouch(0).position.y : joyStickStartPos.y;*/

        Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y));
        float clickDistance = (pos - joyStickStartPos).magnitude;
        Vector2 dirVector = (pos - joyStickStartPos).normalized;
        float maxDistance = 1f;


        if (clickDistance > maxDistance)
        {
            float x = dirVector.x + joyStickStartPos.x;
            float y = dirVector.y + joyStickStartPos.y;
            transform.position = new Vector3(x, y, -.5f);
        }
        else
            transform.position = new Vector3(pos.x, pos.y, -.5f);


        //			float maxY = joystickMaxSpot.y;
        //			float maxX = joystickMaxSpot.x;
        //			
        //			float minY = joystickMinSpot.y;
        //			float minX = joystickMinSpot.x;
        //			
        //			//if inside Max area
        //			if(pos.x < maxX && pos.y < maxY && pos.y > minY && pos.x > minX)
        //			{
        //				transform.position = new Vector3(pos.x,pos.y,-0.5f);
        //			}
        //			
        //			//check X
        //			else if(pos.x > maxX && pos.y < maxY && pos.y > minY && pos.x > minX)
        //			{
        //				transform.position = new Vector3(maxX,pos.y,-0.5f);
        //			}
        //			else if(pos.x < maxX && pos.y < maxY && pos.y > minY && pos.x < minX)
        //			{
        //				transform.position = new Vector3(minX,pos.y,-0.5f);
        //			}
        //			
        //			//Check Y
        //			else if(pos.x < maxX && pos.y > maxY && pos.y > minY && pos.x > minX)
        //			{
        //				transform.position = new Vector3(pos.x,maxY,-0.5f);
        //			}
        //			else if(pos.x < maxX && pos.y < maxY && pos.y < minY && pos.x > minX)
        //			{
        //				transform.position = new Vector3(pos.x,minY,-0.5f);
        //			}
#elif UNITY_STANDALONE
        if(Input.GetMouseButton(0))
			{
				Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y));
				float clickDistance = (pos - joyStickStartPos).magnitude;
				Vector2 dirVector = (pos - joyStickStartPos).normalized;
				float maxDistance = 1f;


				if(clickDistance > maxDistance)
				{
					float x = dirVector.x + joyStickStartPos.x;
					float y = dirVector.y + joyStickStartPos.y;
					transform.position = new Vector3(x,y,-.5f);
				}
				else
					transform.position = new Vector3(pos.x,pos.y,-.5f);
			}
#endif
        return transform.position;
    }
}