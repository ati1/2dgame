﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlaySoundSample : MonoBehaviour {

	private AudioSource aSource;
	public AudioClip[] clips;
	public float playTime;
	private float timer;

	void Awake()
	{
		aSource = GetComponent<AudioSource>();
		aSource.clip = clips[PlayerPrefs.GetInt("SongID")];
	}

	public void PlayAudioSample(int id)
	{
		aSource.clip = clips[id];
		aSource.Stop();
		aSource.Play();
	}

	void Update()
	{
		if(aSource.isPlaying)
		{
			if(timer <= 0)
			{
				timer = playTime;
				aSource.Stop();
			}

			timer -= Time.deltaTime;
		}
	}
}