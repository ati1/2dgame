﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	
	public float moveSpeed = 0;
	public bool enableMovements = true;
	public Camera mainCamera;
	public float score = 0;
	public Rect scorePos = new Rect(0,0,0,0);

	public float colorChange = 0;
	public float maxColorChangeTimer = 0;
	public float colorChangeLerp = 0;

	public GameObject endScreen = null;
	public string levelName = "";
	public bool gameOver = false;
	
	void Update () 
	{
		if (gameOver) 
		{
			if(FindObjectOfType<EnemySpawner>().chekingScoresDone)
				DisableSpawner();
		}
		score += Time.deltaTime;
		CheckIfOutsideView();
	}
	void CheckIfOutsideView()
	{
		if(mainCamera.WorldToViewportPoint(transform.position).x < 0
		   || mainCamera.WorldToViewportPoint(transform.position).x > 1)
			transform.position = new Vector3(-transform.position.x,transform.position.y);
		if(mainCamera.WorldToViewportPoint(transform.position).y < 0
		   || mainCamera.WorldToViewportPoint(transform.position).y > 1)
			transform.position = new Vector3(transform.position.x,-transform.position.y);
	}
	public void GameOver()
	{
		gameOver = true;
		FindObjectOfType<Joystick>().enabled = false;
		Debug.Log("Game Over");
		enableMovements = false;
		endScreen.GetComponent<Animation>().Play();
		GameObject.FindGameObjectWithTag("GameOver").GetComponent<ScoreText>().SetFinalScore((int)score);
		Enemy[] enemys = FindObjectsOfType<Enemy>();
		foreach(Enemy enemy in enemys)
		{
			enemy.enabled = false;
		}
		GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreText>().enabled = false;
		GetComponent<Collider2D>().enabled = false;
	}
	void DisableSpawner()
	{
		FindObjectOfType<EnemySpawner> ().enabled = false;
	}
	public void ReloadLevel()
	{
		Application.LoadLevel(levelName);
	}
	public Color32 GetRandomColor()
	{
		Color32 randomColor = new Color();

		randomColor.r = (byte)Random.Range(0,255);
		randomColor.g = (byte)Random.Range(0,255);
		randomColor.b = (byte)Random.Range(0,255);
		randomColor.a = 255;

		return randomColor;
	}
	public void ChangeColor(Color32 newColor, float colorChangeLerp)
	{
		GetComponent<SpriteRenderer>().color = Color32.Lerp(GetComponent<SpriteRenderer>().color,newColor,colorChangeLerp);
	}
}