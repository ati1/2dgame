﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public bool isActive;
	public float moveSpeed = 0;
	public EnemySpawner spawner;
	public Camera mainCamera;
	[HideInInspector]
	public Vector3 startScale;
    public CollectColor type;

	void Awake()
	{
		startScale = transform.localScale;
		spawner = FindObjectOfType<EnemySpawner>();
		mainCamera = Camera.main;
	}
	
	void Update () 
	{
		transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
		if(mainCamera.WorldToViewportPoint(transform.position).x < -.05f 
		   || mainCamera.WorldToViewportPoint(transform.position).x > 1.05f 
		   || mainCamera.WorldToViewportPoint(transform.position).y < -.05f
		   || mainCamera.WorldToViewportPoint(transform.position).y > 1.05f )
		{

			Remove();
		}
			
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.GetComponent<Movement> ()) 
		{
            if (CollectManager.Instance.CurrentCollectColor == type && CollectManager.Instance.CanCollect)
            {
                col.GetComponent<Movement>().score += 20;
                ShakeCamera.Instance.shake = 0.25f;
                Remove();
            }
            else
                col.GetComponent<Movement>().GameOver();
		}
	}
	void Remove()
	{
		spawner.enemysOnField -= 1;
		isActive = false;
		gameObject.SetActive (false);
	}
}