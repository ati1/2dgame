﻿using UnityEngine;
using System.Collections;

public class ScoreText: MonoBehaviour {

	public string scoreString = "";
	private Movement movement = null;
	public bool updateText = false;

	void Start () 
	{
		movement = FindObjectOfType<Movement>();
	}

	void Update () 
	{
		if(updateText)
			GetComponent<TextMesh>().text = scoreString + (int)movement.score;
	}
	public void SetFinalScore(int score)
	{
		GetComponent<TextMesh>().text = scoreString + (int)movement.score;
		updateText = false;
	}
}