﻿using UnityEngine;
using System.Collections;

public class Collect : MonoBehaviour
{
    public CollectColor cc;
    public float time;

    void FixedUpdate()
    {
        transform.RotateAround(transform.position, transform.forward, Time.deltaTime * 90f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Movement>())
        {
            CollectManager.Instance.SetCollectColor(cc, time);
            CollectManager.Instance.runSpawnTimer = true;
            Destroy(gameObject);
        }
    }
}
