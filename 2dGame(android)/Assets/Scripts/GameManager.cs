﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public enum ManagerType
	{
		Menu,
		Game
	};

	public MusicScript musicManager;
	public Slider difficultyScaler;
	public UnityEngine.UI.Text emotion;

	public int songId;
	public ManagerType type;
	public UnityEngine.UI.Text highScoreMesh;
	public Vector2 joyStickRightPos;
	public Vector2 joyStickLeftPos;
	public static int difficulty = 0;

    [SerializeField]
    private GameObject m_startUI;
    [SerializeField]
    private GameObject m_menuUI;

	public void SetDifficulty()
	{
		PlayerPrefs.SetInt("difficulty",(int)difficultyScaler.value);

		switch((int)difficultyScaler.value)
		{
		case 1:
			emotion.text = ":^)";
			break;
		case 2:
			emotion.text = ":^/";
			break;
		case 3:
			emotion.text = ":^(";
			break;
		/*case 4:
			emotion.text = ":^/";
			break;
		case 5:
			emotion.text = ":^(";
			break;*/
		}
	}
	public void GetDifficulty()
	{
		int difficulty = PlayerPrefs.GetInt ("difficulty");
		
		switch(difficulty)
		{
		case 1:
			difficultyScaler.value = 1;
			emotion.text = ":^)";
			break;
		case 2:
			difficultyScaler.value = 2;
			emotion.text = ":^/";
			break;
		case 3:
			difficultyScaler.value = 3;
			emotion.text = ":^(";
			break;
		/*case 4:
			difficultyScaler.value = 4;
			emotion.text = ":^/";
			break;
		case 5:
			difficultyScaler.value = 5;
			emotion.text = ":^(";
			break;*/
		}
	}

	public void StartGame()
	{
		Application.LoadLevel("Game");
	}

    public void ShowOptions()
    {
        m_startUI.SetActive(false);
        m_menuUI.SetActive(true);
    }

    public void ShowStart()
    {
        m_startUI.SetActive(true);
        m_menuUI.SetActive(false);
        musicManager.folderBrowser.SetActive(false);
    }

    public void QuitGame()
	{
		Application.Quit();
	}

	void Awake()
	{
		if (PlayerPrefs.GetFloat("posX") == 0 && PlayerPrefs.GetFloat("posY") == 0)
		{
			ChangeJoyStickPos(joyStickRightPos);
		}
		difficulty = PlayerPrefs.GetInt("difficulty");
		songId =  PlayerPrefs.GetInt("SongID");

		if(musicManager != null)
			musicManager.aClipIndex = songId;

		if(type == ManagerType.Menu)
		{
			GetHighScores();
			GetDifficulty();
		}
	}

	public void ChangeHand(int id)
	{
		if(id == 0)
			ChangeJoyStickPos(joyStickLeftPos);
		else
			ChangeJoyStickPos(joyStickRightPos);
	}

	void ChangeJoyStickPos(Vector2 pos)
	{
		PlayerPrefs.SetFloat("posX",pos.x);
		PlayerPrefs.SetFloat("y",pos.y);
	}

	public void ResetData()
	{
		PlayerPrefs.DeleteAll();
		ChangeJoyStickPos(joyStickRightPos);
		highScoreMesh.text = "0\n"+"0\n"+"0";
	}

	public void GetHighScores()
	{
		int[] highScores = new int[3]
		{
			PlayerPrefs.GetInt ("HighScore1"),
			PlayerPrefs.GetInt ("HighScore2"),
			PlayerPrefs.GetInt ("HighScore3")
			/*PlayerPrefs.GetInt ("HighScore4"),
			PlayerPrefs.GetInt ("HighScore5")*/
		};

		for(int i = 0;i<highScores.Length;i++)
		{
			highScoreMesh.text += highScores[i].ToString() + "\n"; 
		}
	}
}