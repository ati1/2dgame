﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class SoundFileManager : MonoBehaviour 
{
	public string soundFolderPath = "";
	public static List<AudioClip> s_aClips = new List<AudioClip> ();

	private bool m_checkSubFolders = false;

	[SerializeField]
	private AudioSource m_aPrefab;

	private void Awake()
	{
		//if(s_aClips.Count <= 2)
		//	GetSoundFiles (soundFolderPath);
	}

	public void CreateASource(AudioClip aClip)
	{
		AudioSource aO = GameObject.Instantiate (m_aPrefab, Vector3.zero, Quaternion.identity) as AudioSource;
		aO.clip = aClip;
		aO.Play ();
	}
	public void GetSoundFiles()
	{
		MusicScript ms = FindObjectOfType<MusicScript> ();
		soundFolderPath = ms.iField.text;
		m_checkSubFolders = ms.toggle.isOn;
		GetSoundFiles (soundFolderPath);
	}
	
	public void GetSoundFiles(string path)
	{
		if(soundFolderPath == "")
			return;

		List<string> soundPaths = new List<string>();

		if(m_checkSubFolders)
		{
			foreach(string file in Directory.GetFiles(path,"*.mp3",SearchOption.AllDirectories))
			{
				soundPaths.Add(file);
			}
			/*foreach(string file in Directory.GetFiles(path,"*.ogg",SearchOption.AllDirectories))
			{
				soundPaths.Add(file);
			}*/
			foreach(string file in Directory.GetFiles(path,"*.wav",SearchOption.AllDirectories))
			{
				soundPaths.Add(file);
			}
		}
		else
		{
			foreach(string file in Directory.GetFiles(path))
			{
				if(Path.GetExtension(file).Equals(".mp3") || /*Path.GetExtension(file).Equals(".ogg") ||*/ 
				   Path.GetExtension(file).Equals(".wav"))
				{
					soundPaths.Add(file);
				}
			}
		}

        //TODO: EI NÄIN IKINÄ KEKSI PAREMPI!!!!
		/*foreach(string sPath in soundPaths)
		{
			StartCoroutine("DownloadClip", sPath);
		}*/
	}

	public void ResetSoundList()
	{
		StopAllCoroutines ();
		MusicScript ms = FindObjectOfType<MusicScript> ();
		ms.aClips.RemoveRange (2, ms.aClips.Count - 2);
		ms.slider.minValue = 0;
		ms.slider.maxValue = 1;
		ms.slider.value = 0;
		s_aClips.Clear ();
	}

    public void LoadClip(string path)
    {
        StartCoroutine("DownloadClip", path);
    }

	private IEnumerator DownloadClip(string path)
	{
        //Debug.Log("Started downloading from: " + path);
#if UNITY_STANDALONE
		WWW www = new WWW(@"file:///"+path);
#elif UNITY_ANDROID
		WWW www = new WWW(@"file:///"+path);
#endif
        //yield return www;

		while(!www.isDone)
            yield return null;
        
        AudioClip aClip = www.GetAudioClip(false);

        Debug.Log("Download finished for: " + path);

        aClip.name = Path.GetFileNameWithoutExtension(path); ;
		s_aClips.Add(aClip);

        //yield return null;
		//FindObjectOfType<MusicScript> ().aClips.Add (aClip);
		//CreateASource(aClip);

		www.Dispose ();
	}
}