﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class MenuCamera : MonoBehaviour {
	
	private Transform m_transform;
	private int index;
	public float speed;
	public Vector3[] moveMap;
	public Transform lookAt;
	public float changeDist;
	public Vector3 velocity;

	void Awake()
	{
		m_transform = transform;
	}
	void FixedUpdate()
	{
		MoveCamera();
	}

	void MoveCamera()
	{
		m_transform.LookAt(lookAt);

		if( index == moveMap.Length )
		{
			index = 0;
		}

		m_transform.position = Vector3.SmoothDamp(m_transform.position,moveMap[index],ref velocity,speed);

		if( (m_transform.position - moveMap[index]).sqrMagnitude < changeDist )
		{
			index++;
		}
	}
}