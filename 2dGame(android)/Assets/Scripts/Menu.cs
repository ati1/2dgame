﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Menu : MonoBehaviour {

	[System.Serializable]
	public class MainMenuButton
	{
		public Rect buttonPosition = new Rect(0,0,0,0);
		public Texture buttonTexture = null;
		public string buttonText = "";
		public int buttonID = 0;
		public GUIStyle customStyle = null;
	}
	[System.Serializable]
	public class OptionsSelectButton : MainMenuButton{}

	[SerializeField]
	//private Texture2D titleScecth3;
	public GUISkin customGuiSKin;
	public Rect textAreaRect = new Rect(0,0,0,0);

	public MainMenuButton[] mainMenuButtons = null;
	public OptionsSelectButton[] optionsSelectButtons = null;

	public GameObject logo = null;
	public Vector3 logoPos = new Vector3(0,0,0);

	public bool showMainMenu = false;
	public bool showOptions = false;





	void OnGUI()
	{
		//GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),titleScecth3);

		if(customGuiSKin != null)
			GUI.skin = customGuiSKin;

		if(showMainMenu)
		{
			GUI.BeginGroup(textAreaRect);
			foreach(MainMenuButton button in mainMenuButtons)
			{
				if(button.buttonTexture == null)
				{
					if(GUI.Button(button.buttonPosition,button.buttonText/*,button.customStyle*/))
						DoButtons(button.buttonID);
				}
				else 
				{
					if(GUI.Button(button.buttonPosition,button.buttonTexture/*,button.customStyle*/))
						DoButtons(button.buttonID);
				}
			}
			GUI.EndGroup();
		}
		if(showOptions)
		{
			GUI.BeginGroup(textAreaRect);
			foreach(OptionsSelectButton button in optionsSelectButtons)
			{
				if(button.buttonTexture == null)
				{
					if(GUI.Button(button.buttonPosition,button.buttonText/*,button.customStyle*/))
						DoButtons(button.buttonID);
				}
				else 
				{
					if(GUI.Button(button.buttonPosition,button.buttonTexture/*,button.customStyle*/))
						DoButtons(button.buttonID);
				}
			}
			GUI.EndGroup();
		}
	}
	void DoButtons(int buttonID)
	{
		//do main menu buttons
		if(showMainMenu)
		{
			if(buttonID == 0)
				Application.LoadLevel("Game");
			if(buttonID == 1)
				ShowMenu(1);
			if(buttonID == 2)
				ShowMenu(2);
		}
		//do options select buttons
		else if(showOptions)
		{
			if(buttonID == 0)
				ShowMenu(0);
		}
	}
	public void ShowMenu(int menuID)
	{
		switch (menuID)
		{
			case 0: showMainMenu = true; showOptions = false; break;
			case 1: showMainMenu = false; showOptions = true; break;
			case 2: Application.Quit(); break;
		}
	}
}