﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class BgColor : MonoBehaviour {

	public Color color;

	void Start()
	{
		gameObject.GetComponent<SpriteRenderer>().sharedMaterial.color = color;
	}
}
