﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

	public GameObject joystick;
	//public List<Enemy> currentEnemies;

	public bool debugMode;

	public enum Difficulty
	{
		Faceroll,
		Meh,
		KeepGoing,
		DontTry,
		Impossible
	};

	public Difficulty difficulty;

	public Camera mainCamera;
	public List<Enemy> enemyList = new List<Enemy>();

	public float maxEnemysOnField = 0;
	public float enemysOnField = 0;

	public float spawnTimer = 0;
	public float maxSpawnTimer = 0;

	public float enemyMinSpeed = 0;
	public float enemyMaxSpeed = 0;
	public bool chekingScoresDone = false;

	public int enemyAmount;
	public List<Enemy> enemyPool;

	void Start()
	{
		SetGameMode();
		InitPool ();
	}

	void InitPool()
	{
		//Fill object pool with different enemies.
		for( int i = 0;i<enemyAmount;i++ )
		{
			int random = Random.Range(0,enemyList.Count);
			Enemy currentEnemy = enemyList[random];
			GameObject temp = GameObject.Instantiate(currentEnemy.gameObject,Vector3.zero,Quaternion.identity) as GameObject;
			enemyPool.Add(temp.GetComponent<Enemy>());
			temp.SetActive(false);
		}
	}
	public void SpawnEnemyFromUp(int enemyIndex)
	{
		Enemy temp = enemyPool [enemyIndex];
		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(Random.value,1));
		temp.transform.position = new Vector3(pos.x,pos.y,0);
		temp.moveSpeed = Random.Range (enemyMinSpeed,enemyMaxSpeed);
        temp.transform.rotation = Quaternion.identity;
		temp.transform.Rotate(new Vector3(0,0,Random.Range(150,225)));
		temp.gameObject.SetActive (true);
		temp.isActive = true;

//		newEnemy.moveSpeed = Random.Range(enemyMinSpeed,enemyMaxSpeed);
//		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(Random.value,1));
//		GameObject curObj = null;
//		curObj = GameObject.Instantiate(newEnemy.gameObject,pos,Quaternion.identity) as GameObject;
//		curObj.transform.Rotate(new Vector3(0,0,Random.Range(150,225)));
//		currentEnemies.Add (curObj.GetComponent<Enemy>());
	}
	public void SpawnEnemyFromLeft(int enemyIndex)
	{
		Enemy temp = enemyPool [enemyIndex];
		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(1,Random.value));
		temp.transform.position = new Vector3(pos.x,pos.y,0);
		temp.moveSpeed = Random.Range (enemyMinSpeed,enemyMaxSpeed);
        temp.transform.rotation = Quaternion.identity;
        temp.transform.Rotate(new Vector3(0,0,Random.Range(30,145)));
		temp.gameObject.SetActive (true);
		temp.isActive = true;

//		newEnemy.moveSpeed = Random.Range(enemyMinSpeed,enemyMaxSpeed);
//		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(1,Random.value));
//		GameObject curObj = null;
//		curObj = GameObject.Instantiate(newEnemy.gameObject,pos,Quaternion.identity) as GameObject;
//		curObj.transform.Rotate(new Vector3(0,0,Random.Range(30,145)));
//		currentEnemies.Add (curObj.GetComponent<Enemy>());
	}
	public void SpawnEnemyFromRight(int enemyIndex)
	{
		Enemy temp = enemyPool [enemyIndex];
		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(0,Random.value));
		temp.transform.position = new Vector3(pos.x,pos.y,0);
		temp.moveSpeed = Random.Range (enemyMinSpeed,enemyMaxSpeed);
        temp.transform.rotation = Quaternion.identity;
        temp.transform.Rotate(new Vector3(0,0,Random.Range(-30,-145)));
		temp.gameObject.SetActive (true);
		temp.isActive = true;

//		newEnemy.moveSpeed = Random.Range(enemyMinSpeed,enemyMaxSpeed);
//		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(0,Random.value));
//		GameObject curObj = null;
//		curObj = GameObject.Instantiate(newEnemy.gameObject,pos,Quaternion.identity) as GameObject;
//		curObj.transform.Rotate(new Vector3(0,0,Random.Range(-30,-145)));
//		currentEnemies.Add (curObj.GetComponent<Enemy>());
	}
	public void SpawnEnemyFromDown(int enemyIndex)
	{
		Enemy temp = enemyPool [enemyIndex];
		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(Random.value,0));
		temp.transform.position = new Vector3(pos.x,pos.y,0);
		temp.moveSpeed = Random.Range (enemyMinSpeed,enemyMaxSpeed);
        temp.transform.rotation = Quaternion.identity;
        temp.transform.Rotate(new Vector3(0,0,Random.Range(-55,-55)));
		temp.gameObject.SetActive (true);
		temp.isActive = true;

//		newEnemy.moveSpeed = Random.Range(enemyMinSpeed,enemyMaxSpeed);
//		Vector2 pos = mainCamera.ViewportToWorldPoint(new Vector2(Random.value,0));
//		GameObject curObj = null;
//		curObj = GameObject.Instantiate(newEnemy.gameObject,pos,Quaternion.identity) as GameObject;
//		curObj.transform.Rotate(new Vector3(0,0,Random.Range(-55,55)));
//		currentEnemies.Add (curObj.GetComponent<Enemy>());
	}
	void Update () 
	{
		Spawn ();
	}
	void Spawn()
	{
		if (FindObjectOfType<Movement> ().gameOver && !chekingScoresDone)
			GameOver ();

		int random = Random.Range(0,enemyPool.Count);
		
		if(enemysOnField < maxEnemysOnField && spawnTimer >= maxSpawnTimer && !enemyPool[random].isActive)
		{
			int selection = Random.Range(1,5);
			switch(selection)
			{
			case 1: SpawnEnemyFromUp(random);
				break;
			case 2: SpawnEnemyFromLeft(random);
				break;
			case 3: SpawnEnemyFromRight(random);
				break;
			case 4: SpawnEnemyFromDown(random);
				break;
			}
			spawnTimer = 0;
			enemysOnField += 1;
		}
		
		if(spawnTimer >= maxSpawnTimer)
			spawnTimer = maxSpawnTimer;
		spawnTimer += Time.deltaTime;
	}
	public void SetGameMode()
	{
		if(debugMode)
			return;

		switch(PlayerPrefs.GetInt("difficulty"))
		{
            /*case 0: difficulty = Difficulty.Faceroll;break;
            case 2: difficulty = Difficulty.Meh;break;
            case 3: difficulty = Difficulty.KeepGoing;break;
            case 4: difficulty = Difficulty.DontTry;break;
            case 5: difficulty = Difficulty.Impossible;break;*/
            case 1:
                difficulty = Difficulty.Faceroll;
                break;
            case 2:
                difficulty = Difficulty.KeepGoing;
                break;
            case 3:
                difficulty = Difficulty.Impossible;
                break;

        }

		switch(difficulty)
		{
		case Difficulty.Faceroll: Easy(); break;
		//case Difficulty.Meh: Easier(); break;
		case Difficulty.KeepGoing: Medium(); break;
		//case Difficulty.DontTry: Hard(); break;
		case Difficulty.Impossible: Impossible(); break;
		}

	}
	void Easy()
	{
		if(joystick.GetComponent<Joystick>())
			joystick.GetComponent<Joystick>().speed = 5f;
		enemyAmount = 25;
		maxEnemysOnField = 15;
		enemyMinSpeed = .7f;
		enemyMaxSpeed = 1.5f;
		maxSpawnTimer = .5f;
	}
	void Easier()
	{
		if(joystick.GetComponent<Joystick>())
			joystick.GetComponent<Joystick>().speed = 5f;
		enemyAmount = 25;
		maxEnemysOnField = 15;
		enemyMinSpeed = .7f;
		enemyMaxSpeed = 2f;
		maxSpawnTimer = .4f;
	}
	void Medium()
	{
		if(joystick.GetComponent<Joystick>())
			joystick.GetComponent<Joystick>().speed = 5f;
		enemyAmount = 25;
		maxEnemysOnField = 15;
		enemyMinSpeed = 1.1f;
		enemyMaxSpeed = 2.2f;
		maxSpawnTimer = .25f;
	}
	void Hard()
	{
		if(joystick.GetComponent<Joystick>())
			joystick.GetComponent<Joystick>().speed = 5f;
		enemyAmount = 40;
		maxEnemysOnField = 25;
		enemyMinSpeed = 1.5f;
		enemyMaxSpeed = 2.1f;
		maxSpawnTimer = .2f;
	}
	void Impossible()
	{
		if(joystick.GetComponent<Joystick>())
			joystick.GetComponent<Joystick>().speed = 5f;
		enemyAmount = 40;
		maxEnemysOnField = 30;
		enemyMinSpeed = .9f;
		enemyMaxSpeed = 2.1f;
		maxSpawnTimer = .15f;
	}
	void GameOver()
	{
		int score = (int)FindObjectOfType<Movement> ().score;
		switch(difficulty)
		{
		case Difficulty.Faceroll: 
			 CheckScore(score,1); 
			 break;
		/*case Difficulty.Meh: 
			 CheckScore(score,2); 
			 break;*/
		case Difficulty.KeepGoing: 
			 CheckScore(score,2); 
			 break;
		/*case Difficulty.DontTry:
			 CheckScore(score,4); 
			 break;*/
		case Difficulty.Impossible: 
			 CheckScore(score,3); 
			 break;
		}
		chekingScoresDone = true;

	}
	void CheckScore(int score, int highScoreID)
	{
		if ((int)score > PlayerPrefs.GetInt ("HighScore" + highScoreID.ToString ())) 
		{
			PlayerPrefs.SetInt ("HighScore"+highScoreID.ToString(), (int)score);
		}
	}
}