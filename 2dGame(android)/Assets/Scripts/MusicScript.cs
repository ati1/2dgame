﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MusicScript : MonoBehaviour 
{
	public Slider slider;
	public InputField iField;
	public Toggle toggle;
	public UnityEngine.UI.Text text;
    public GameObject folderBrowser;

	public void SaveEnteredText()
	{
		PlayerPrefs.SetString ("iText", iField.text);
	}
	
	public void ChangeSong()
	{
		aClipIndex = (int)slider.value;
		PlayerPrefs.SetInt("SongID",(int)slider.value);
		Start();
	}

	public void SaveToggle()
	{
		PlayerPrefs.SetInt("toggle", toggle.isOn ? 0 : 1);
	}

    public void ToggleFolderBrowser()
    {
        bool activate = folderBrowser.activeSelf ? false : true;
        folderBrowser.SetActive(activate);
    }

	private void FixedUpdate()
	{
		check();
		if(spawner != null)
			ScaleObjects ();
	}
	
	public EnemySpawner spawner;
	
	public int numOfSamples = 8192; //Min: 64, Max: 8192
	
	public AudioSource aSource;
	
	public List<AudioClip> aClips;
	public int aClipIndex;
	
	private float[] freqData;
	private float[] band;
	
	public GameObject[] scaleObjects;

    public float bandAvg;

	private void Awake()
	{
		aClipIndex = PlayerPrefs.GetInt("SongID");

		if(slider != null)
			slider.value = aClipIndex;

		if(iField != null)
		{
			iField.text = PlayerPrefs.GetString("iText");
			SoundFileManager sfm = FindObjectOfType<SoundFileManager>();

			if(sfm != null)
			{
				sfm.soundFolderPath = iField.text;
			}
		}

		if(toggle != null)
		{
			int i = PlayerPrefs.GetInt("toggle");

			if(i == 0)
				toggle.isOn = false;
			else
				toggle.isOn = true;
		}
	}

	private void UpdateAClipList()
	{
		if((aClips.Count - 2) < SoundFileManager.s_aClips.Count)
			aClips.AddRange (SoundFileManager.s_aClips);
	}

	private void Start ()
	{
		UpdateAClipList ();

		if(slider != null)
			slider.maxValue = aClips.Count - 1;

		aSource.clip = aClips[aClipIndex];

		if(text != null)
			text.text = aClips[aClipIndex].name;

		aSource.Play();
		
		freqData = new float[numOfSamples];
		
		int n  = freqData.Length;
		
		int k = 0;
		for(int j = 0; j < freqData.Length; j++)
		{
			n = n / 2;
			if(n <= 0) 
				break;
			k++;
		}
		
		band  = new float[k+1];
		if(scaleObjects.Length > 0)
		{
			for( int i = 0;i<scaleObjects.Length;i++)
			{
				Destroy(scaleObjects[i].gameObject);
			}
		}

		scaleObjects     = new GameObject[k+1];

		SpawnObjects();

        // update at 15 fps
        //InvokeRepeating("check", 0.0f, 1.0f/15.0f);
    }
	private void SpawnObjects()
	{
		for (int i = 0; i < band.Length; i++)
		{
			band[i] = 0;
			scaleObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
			scaleObjects[i].GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
			scaleObjects[i].layer = 8;
			scaleObjects[i].transform.position = new Vector3(-5f + i, 0, 5);
			scaleObjects[i].transform.eulerAngles = new Vector3(-25,scaleObjects[i].transform.eulerAngles.y,scaleObjects[i].transform.eulerAngles.z);
			
			if(i == 0 || i == band.Length - 1)
			{
				scaleObjects[i].SetActive(false);
			}
		}
	}
	
	private void check()
	{
		aSource.GetSpectrumData(freqData, 0, FFTWindow.Rectangular);
		
		int k = 0;
		int crossover = 2;
		
		for (int i = 0; i < freqData.Length; i++)
		{
			float d = freqData[i];
			float b = band[k];
			
			// find the max as the peak value in that frequency band.
			band[k] = ( d > b )? d : b;

			if (i > (crossover-3) )
			{
				k++;
				// frequency crossover point for each band.
				crossover *= 2;   
				Vector3 tmp = new Vector3 (scaleObjects[k].transform.localScale.x, band[k]*32, scaleObjects[k].transform.localScale.z);
				scaleObjects[k].transform.localScale = tmp;
				//g[k].transform.localScale = Vector3.Lerp(g[k].transform.localScale,tmp,1f);
				band[k] = 0;
			}
        }

        float temp = 0;
        foreach(float f in band)
            temp += f;
        bandAvg = temp / band.Length;
        bandAvg = (bandAvg >= 0.1f) ? 0.1f : bandAvg;
    }
	
	public void ScaleObjects()
	{
		for(int i = 0;i<spawner.enemyPool.Count;i++)
		{
			if(spawner.enemyPool[i] != null)
			{
				Transform obj = spawner.enemyPool[i].transform;
				float sizeX = spawner.enemyPool[i].startScale.x;
				float sizeY = spawner.enemyPool[i].startScale.y;

				Vector3 temp = new Vector3(sizeX + (bandAvg * 1.5f),sizeY + (bandAvg * 1.5f), obj.localScale.z);
				obj.localScale = Vector3.Lerp(obj.localScale,temp,.2f);
			}
		}
	}
}