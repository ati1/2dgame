﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickUpSpawner : MonoBehaviour {

	public Transform lastSpawnSpot;
	public GameObject[] objectsToSpawn;
	public Transform[] spawnSpots;

	public bool basedOnSceneAmount;
	public int[] amounts;
	public int index;

	public bool basedOnTime;
	public float maxTimer;
	private float m_timer;

	public void SpawnPickUp(int objIndex,int spawnIndex)
	{
		GameObject.Instantiate ( objectsToSpawn [objIndex], spawnSpots [spawnIndex].position, Quaternion.identity );
	}

	void Update()
	{
		if(basedOnTime)
			BasedOnTime();

		if(basedOnSceneAmount)
			BasedOnSceneAmount();

	}

	void BasedOnSceneAmount()
	{

	}

	void BasedOnTime()
	{
		if(m_timer <= 0)
		{
			int randomObject = Random.Range(0,objectsToSpawn.Length);
			int randomSpot = Random.Range(0,spawnSpots.Length);
			
			if(lastSpawnSpot != spawnSpots[randomSpot])
			{
				SpawnPickUp(randomObject,randomSpot);
			}
			else
			{
				SpawnPickUp(randomObject,randomSpot + 1);
			}
			
			m_timer = maxTimer;
		}
		
		m_timer -= Time.deltaTime;
	}
}