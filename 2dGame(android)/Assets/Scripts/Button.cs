﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class Button : MonoBehaviour {

	public GameObject buttonObj = null;
	public Vector3 buttonPos;
	public TextMesh buttonText = null;
	public bool isPressed = false;
	public int buttonId = 0;

	void Update()
	{
		buttonPos = transform.position;
	}
}